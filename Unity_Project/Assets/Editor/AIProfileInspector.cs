﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AIProfile))]
public class AIProfileInspector : Editor
{
    AIProfile script;

    private void OnEnable()
    {
        script = target as AIProfile;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Random Profile"))
        {
            script.SetRandomValues();
        }
    }
}
