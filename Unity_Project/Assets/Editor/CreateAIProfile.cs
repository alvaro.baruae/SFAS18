﻿using UnityEngine;
using UnityEditor;

public class MakeAvatarData
{
    [MenuItem("Assets/Create/AI Profile")]
    public static void CreateMyAsset()
    {
        AIProfile asset = ScriptableObject.CreateInstance<AIProfile>();

        AssetDatabase.CreateAsset(asset, "Assets/Resources/AI Profiles/AIProfile.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
