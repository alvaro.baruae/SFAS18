﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(AudioManager))]
public class AudioManagerEditor : Editor
{
    private ReorderableList m_AudioList;

    public void OnEnable()
    {
        m_AudioList = new ReorderableList(serializedObject, serializedObject.FindProperty("m_AudioTypePairList"), false, true, true, true);
        m_AudioList.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Audio Pairs");
        };

        m_AudioList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = m_AudioList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, 100, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("type"), GUIContent.none);
            EditorGUI.PropertyField(new Rect(rect.x + 115, rect.y, rect.width - 115, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("clip"), GUIContent.none);
        };
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.Space();
        serializedObject.Update();
        m_AudioList.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }
}
