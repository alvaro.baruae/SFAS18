﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;


[CustomEditor(typeof(UIManager))]
public class UIManagerEditor : Editor {

    private ReorderableList m_SpritesList;
    private bool m_bShowAchievementsConfig = false;

    public void OnEnable()
    {
        m_SpritesList = new ReorderableList(serializedObject, serializedObject.FindProperty("m_AchievementSpritePairList"), false, true, true, true);
        m_SpritesList.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Achievements Sprite Pairs");
        };

        m_SpritesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = m_SpritesList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, 100, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("achievement"), GUIContent.none);
            EditorGUI.PropertyField(new Rect(rect.x + 115, rect.y, rect.width - 115, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("sprite"), GUIContent.none);
        };
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space();
        m_bShowAchievementsConfig = EditorGUILayout.Foldout(m_bShowAchievementsConfig, "Achievements Configuration");
        if (m_bShowAchievementsConfig)
        {
            serializedObject.Update();
            m_SpritesList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
