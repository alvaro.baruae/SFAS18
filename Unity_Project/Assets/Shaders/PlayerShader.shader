﻿Shader "Custom/PlayerShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Dissolve", 2D) = "white" {}
		_Cutout ("Cutout", Range(0,1)) = 0.5
	}
	SubShader {
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 200
		ZTest LEqual

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alpha:auto
		#pragma target 3.0

		struct Input {
			float2 uv_MainTex;
		};

		sampler2D _MainTex;
		fixed _Cutout;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			if (c.r <= _Cutout)
				discard;
			
			o.Albedo = _Color.rgb;
			o.Smoothness = 0.5;
			o.Alpha = _Color.a;
		}
		ENDCG

		ZTest Greater

		CGPROGRAM
		#pragma surface surf Standard alpha:auto
		#pragma target 3.0

			struct Input {
			float2 uv_MainTex;
			float3 viewDir;
		};

		sampler2D _MainTex;
		fixed _Cutout;
		fixed4 _Color;

		void surf(Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			if (c.r <= _Cutout)
				discard;

			o.Albedo = _Color.rgb * 0.5;
			o.Alpha = _Color.a;

			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission = _Color.rgb * pow(rim, 2);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
