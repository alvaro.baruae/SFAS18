﻿Shader "Custom/Ground Pound Colored" 
{
	Properties
	{
		_WaveDistance("Wave Distance", float) = 1.0
		_Color("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_WaveColor("Wave Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 300

		CGPROGRAM
		#include "EasingFunctions.cginc"
		#pragma surface surf Standard fullforwardshadows addshadow vertex:vert 
		#pragma target 3.0

		fixed ratio;
		fixed _WaveDistance;
		const fixed3 center = float3(0, 0, 0);

		struct Input 
		{
			fixed lerpValue;
		};

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			fixed3 vertexPositionWorld = mul(unity_ObjectToWorld, v.vertex);
			fixed dist = distance(vertexPositionWorld, center);

			if (dist > _WaveDistance - 1 && dist < _WaveDistance + 1)
			{
				ratio = EaseInOut(dist - (_WaveDistance - 1));
				if (ratio > 1)
					ratio = EaseInOut((_WaveDistance + 1) - dist);
				v.vertex.y += ratio;
				o.lerpValue = ratio;
			}
		}

		fixed4 _Color;
		fixed4 _WaveColor;

		void surf(Input IN, inout SurfaceOutputStandard o) 
		{
			o.Albedo = lerp(_Color.rgb, _WaveColor.rgb, IN.lerpValue);
		}
		ENDCG
	}

	FallBack "Diffuse"
}