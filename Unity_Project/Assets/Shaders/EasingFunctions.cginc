float EaseIn(float ratio)
{
	return ratio * ratio * ratio;
}

float EaseOut(float ratio)
{
	float invRatio = ratio - 1.0;
	return (invRatio * invRatio * invRatio) + 1.0;
}

float EaseInOut(float ratio)
{
	if (ratio < 0.5)
		return 0.5 * EaseIn(ratio * 2.0);
	else
		return 0.5 * EaseOut((ratio - 0.5) * 2.0) + 0.5;
}