﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Achievement
{
    King_Of_The_Pill,
    Welcome_To_Capsule_Bash,
};

public class AchievementsManager
{
    private static AchievementsManager m_Instance;
    private Dictionary<Achievement, bool> m_UnlockStates = new Dictionary<Achievement, bool>();

    public static AchievementsManager Instance
    {
        get
        {
            return m_Instance ?? (m_Instance = new AchievementsManager());
        }
    }

    private AchievementsManager()
    {
        //Load from data -- Not saving or loading for demostration purposes.
        m_UnlockStates[Achievement.King_Of_The_Pill] = false;
        m_UnlockStates[Achievement.Welcome_To_Capsule_Bash] = false;

        // Unlock state holds true when unlocked until next game launch.
    }

    public bool CheckUnlockedStatus(Achievement achievement)
    {
        return m_UnlockStates[achievement];
    }

    public void OnAchievementUnlocked(Achievement achievement)
    {
        if (CheckUnlockedStatus(achievement)) return;

        m_UnlockStates[achievement] = true;
        UIManager.Instance.DisplayUnlockedAchievement(achievement);
        //Reward if any 
    }
}
