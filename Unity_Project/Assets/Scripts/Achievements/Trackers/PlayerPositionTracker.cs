﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionTracker : AchievementsSubject
{
    private class TrackedPair
    {
        public float timeStart;
        public PlayerController player;
    };

    [SerializeField] float m_TimeInSpace = 5;
    List<TrackedPair> m_TrackedPlayers = new List<TrackedPair>();

    void Awake()
    {
        InitializeSubject(DestructionBehaviour.GameObject);
    }

    void Update()
    {
        foreach(TrackedPair pair in m_TrackedPlayers)
        {
            if (Time.time - pair.timeStart >= m_TimeInSpace)
                UnlockAchievement();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Player Layer
        if(other.gameObject.layer == 11)
        {
            PlayerController pController = other.gameObject.GetComponent<PlayerController>();
            if (!pController.IsAIControlled)
                m_TrackedPlayers.Add(new TrackedPair() { timeStart = Time.time, player = pController });
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Player Layer
        if (other.gameObject.layer == 11)
        {
            PlayerController pController = other.gameObject.GetComponent<PlayerController>();
            TrackedPair pairToRemove = m_TrackedPlayers.Find(x => x.player == pController);
            if (pairToRemove != null)
                m_TrackedPlayers.Remove(pairToRemove);
        }
    }
}
