﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsSubject : MonoBehaviour
{
    protected enum DestructionBehaviour
    {
        Class,
        GameObject,
        None
    }

    [SerializeField]
    protected Achievement m_TrackedAchievement;
    private DestructionBehaviour m_DestructionBehaviour;
    private AchievementsManager m_AchievementManager = null;

    protected void InitializeSubject(DestructionBehaviour behaviour)
    {
        m_DestructionBehaviour = behaviour;

        m_AchievementManager = AchievementsManager.Instance;
        if (m_AchievementManager.CheckUnlockedStatus(m_TrackedAchievement))
            SelfDestruct();
    }

    protected void UnlockAchievement()
    {
        m_AchievementManager.OnAchievementUnlocked(m_TrackedAchievement);
        SelfDestruct();
    }

    private void SelfDestruct()
    {
        if (m_DestructionBehaviour == DestructionBehaviour.None) return;
        if (m_DestructionBehaviour == DestructionBehaviour.Class)
            Destroy(this);
        else
            Destroy(gameObject);
    }
}