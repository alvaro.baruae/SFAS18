﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSetup : MonoBehaviour {

    [SerializeField] AIController m_AIController;
    [SerializeField] PlayerController m_PlayerController;
    [SerializeField] List<BaseBox> m_Boxes = new List<BaseBox>();
    public static PlayerController TestEnemy;

    private void Awake()
    {
        TestEnemy = m_PlayerController;
    }

    void Start () {
		foreach(BaseBox box in m_Boxes)
        {
            if(box.GetComponent<WoodBox>() != null)
                box.SetUpBox(BoxType.Wood);
            if (box.GetComponent<TNTBox>() != null)
                box.SetUpBox(BoxType.TNT);
            if (box.GetComponent<NitroBox>() != null)
                box.SetUpBox(BoxType.Nitro);
        }

        m_AIController.SetAlive();
        m_PlayerController.SetAlive();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
