﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections;

#if UNITY_EDITOR
using UnityEngine.SceneManagement;
#endif

public class AIController : PlayerController
{
#if UNITY_EDITOR
    public string DEBUG_STATE = string.Empty;
    public
#endif

    AIProfile m_CurrentProfile = null;
    BaseBox m_TargetBox = null;
    Action m_CurrentState = null;
    Transform m_BoxesParent = null;
    PlayerController m_TargetPlayer = null;

    int m_BoxesInLastSeach = 0;
    int m_BoxesMask = 1 << 9;
    bool m_bOnDirectionForLeavingBox = false;
    bool m_AttempedToEvadeWave = false;
    Vector3 m_NitroEvadeDirection = Vector3.zero;

    protected override void Awake()
    {
        base.Awake();
        m_bIsAIControlled = true;
        m_BoxesParent = GameObject.Find("Boxes").transform;
    }

    protected override void Update()
    {
        base.Update();
        if (m_CurrentState != null)
            m_CurrentState();
    }

    public override void SetAlive()
    {
        base.SetAlive();
        m_CurrentState = SearchBox;
    }

    void SearchBox()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "SEARCH BOX";
#endif
        float min = float.MaxValue;
        for (int i = 0; i < m_BoxesParent.childCount; ++i)
        {
            Transform box = m_BoxesParent.GetChild(i);
            BaseBox baseBox = box.GetComponent<BaseBox>();
            if (baseBox.BoxType != BoxType.Nitro)
            {
                float distance = Vector3.Distance(transform.position, box.position);
                distance -= (baseBox.BoxType == BoxType.Wood ? m_CurrentProfile.WoodBoxPriority : m_CurrentProfile.TNTBoxPriority);

                if (distance <= min)
                {
                    min = distance;
                    m_TargetBox = baseBox;
                }
            }
        }

        if (m_TargetBox != null)
        {
            m_BoxesInLastSeach = m_BoxesParent.childCount;
            m_CurrentState = MoveToBox;
        }
        else
        {
            m_HorizontalAxis = 0f;
            m_VerticalAxis = 0f;
        }
    }

    void MoveToBox()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "MOVE TO BOX";
#endif

        if (m_PickedUpBox != null)
        {
            m_CurrentState = SearchOpponent;
            return;
        }

        if (m_BoxesInLastSeach != m_BoxesParent.childCount)
        {
            m_CurrentState = SearchBox;
            return;
        }

        if (m_TargetBox != null && m_TargetBox.IsPickable)
        {
            Vector3 direction = m_TargetBox.transform.position - transform.position;
            direction.Normalize();

            if (direction.y <= -0.85f)
            {
                if (!m_bOnDirectionForLeavingBox)
                {
                    Vector2 random = ExtensionMethods.OnUnitCircle();
                    m_HorizontalAxis = random.x;
                    m_VerticalAxis = random.y;
                }
                m_bOnDirectionForLeavingBox = true;
            }
            else if (direction.y >= -0.25f)
                m_bOnDirectionForLeavingBox = false;

            if (!m_bOnDirectionForLeavingBox)
            {
                m_HorizontalAxis = direction.x;
                m_VerticalAxis = direction.z;
            }

            if (m_CurrentProfile.CanJumpNitro)
            {
                RaycastHit hit;
                if (Physics.SphereCast(transform.position - Vector3.up, 0.5f, direction, out hit, 0.5f, m_BoxesMask))
                {
                    if (hit.collider.GetComponent<BaseBox>().BoxType == BoxType.Nitro && m_CharacterController.isGrounded)
                    {
                        m_bJumpInput = true;
                    }
                }
            }

            if (Vector3.Distance(m_TargetBox.transform.position, transform.position) <= 1.5f)
            {
                m_CurrentState = GrabBox;
            }
        }
        else
        {
            m_TargetBox = null;
            m_CurrentState = SearchBox;
        }
    }

    void GrabBox()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "GRAB BOX";
#endif

        m_FireInput = true;
        if (m_PickedUpBox != null)
            m_CurrentState = SearchOpponent;
        else
            m_CurrentState = MoveToBox;
    }

    void SearchOpponent()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "SEARCH OPPONNENT";
#endif

        if (m_PickedUpBox == null)
        {
            m_TargetBox = null;
            m_CurrentState = SearchBox;
            return;
        }

#if UNITY_EDITOR
        if (SceneManager.GetActiveScene().name == "AI_Test")
        {
            m_TargetPlayer = TestSetup.TestEnemy;
        }
        else
        {
#endif
            float min = float.MaxValue;
            foreach (PlayerController pController in GameManager.PlayerList)
            {
                if (pController != this)
                {
                    if (pController.IsAlive && !pController.IsInvulnerable)
                    {
                        float distance = Vector3.Distance(transform.position, pController.transform.position);
                        if (distance <= min)
                        {
                            min = distance;
                            m_TargetPlayer = pController;
                        }
                    }
                }
            }
#if UNITY_EDITOR
        }
#endif

        if (m_TargetPlayer != null)
            m_CurrentState = MoveToOpponent;
        else
        {
            m_HorizontalAxis = 0f;
            m_VerticalAxis = 0f;
        }
    }

    void MoveToOpponent()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "MOVE TO OPPONNENT";
#endif

        if (m_PickedUpBox == null)
        {
            m_TargetBox = null;
            m_CurrentState = SearchBox;
            return;
        }

        if (m_TargetPlayer != null && !m_TargetPlayer.IsInvulnerable)
        {
            Vector3 direction = m_TargetPlayer.transform.position - transform.position;
            direction.Normalize();

            RaycastHit[] hits;
            if(m_NitroEvadeDirection == Vector3.zero)
                hits = Physics.SphereCastAll(transform.position - Vector3.up, 1f, direction, 2f, m_BoxesMask);
            else
                hits = Physics.SphereCastAll(transform.position - Vector3.up, 1f, direction, Vector3.Distance(m_TargetPlayer.transform.position, transform.position), m_BoxesMask);

            //Only collided with m_PickedUpBox
            if (hits.Length == 2)
                m_NitroEvadeDirection = Vector3.zero;

            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.gameObject != m_PickedUpBox.gameObject && m_CharacterController.isGrounded)
                {
                    if(hit.collider.GetComponent<BaseBox>().BoxType != BoxType.Nitro)
                    {
                        if(Vector3.Distance(transform.position - Vector3.up, hit.transform.position) <= 2.5f)
                        {
                            m_NitroEvadeDirection = Vector3.zero;
                            m_bJumpInput = true;
                        }
                    }
                    else if(m_CurrentProfile.CanEvadeNitroWhileCarryingBoxes && m_NitroEvadeDirection == Vector3.zero)
                    {
                        RaycastHit changeHit;
                        if(!Physics.SphereCast(transform.position - Vector3.up, 0.5f, transform.right, out changeHit, 2f))
                        {
                            m_NitroEvadeDirection = new Vector3(transform.right.x, transform.right.z);
                        }
                        else if(!Physics.SphereCast(transform.position - Vector3.up, 0.5f, -transform.right, out changeHit, 2f))
                        {
                            m_NitroEvadeDirection = new Vector3(-transform.right.x, -transform.right.z);
                        }
                    }
                }
            }

            if(m_NitroEvadeDirection == Vector3.zero)
            {
                m_HorizontalAxis = direction.x;
                m_VerticalAxis = direction.z;
            }
            else
            {
                m_HorizontalAxis = m_NitroEvadeDirection.x;
                m_VerticalAxis = m_NitroEvadeDirection.z;
            }

            if (Vector3.Distance(m_TargetPlayer.transform.position, transform.position) <= Random.Range(3f, 7f))
                m_CurrentState = Throw;
            else
                m_CurrentState = SearchOpponent;
        }
        else
        {
            m_TargetPlayer = null;
            m_CurrentState = SearchOpponent;
        }
    }

    void Throw()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "THROW";
#endif

        if (m_PickedUpBox != null)
        {
            int random = Random.Range(0, 101);
            if (random <= 100 - m_CurrentProfile.AimAccuracy)
            {
                float deviation = Random.Range(-15, 15);
                transform.Rotate(Vector3.up, deviation);
            }

            m_FireInput = false;
            m_TargetBox = null;
            m_TargetPlayer = null;
            m_CurrentState = SearchBox;
        }
    }

    void Evade()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "EVADE";
#endif

        if (m_TargetBox != null)
        {
            Vector3 boxMoveDirection = m_TargetBox.GetComponent<Rigidbody>().velocity.normalized;
            transform.rotation = Quaternion.LookRotation(new Vector3(-boxMoveDirection.x, 0f, -boxMoveDirection.z));

            Vector3 direction = m_TargetBox.transform.position - transform.position;
            bool onFront = Vector3.Dot(transform.forward, direction) > 0;
            float angle = Vector3.SignedAngle(transform.position, direction, Vector3.up);

            if ((angle > 0 && onFront) || (!onFront && angle < 0))
            {
                m_HorizontalAxis = transform.right.x;
                m_VerticalAxis = transform.right.z;
            }
            else
            {
                m_HorizontalAxis = -transform.right.x;
                m_VerticalAxis = -transform.right.z;
            }
        }
        else
        {
            if (m_PickedUpBox == null)
                m_CurrentState = SearchBox;
            else
                m_CurrentState = SearchOpponent;
        }
    }

    void Charge()
    {
#if UNITY_EDITOR
        DEBUG_STATE = "CHARGE";
#endif

        if (!m_bIsCharging && !m_bChargeInput)
        {
            Vector3 direction = m_TargetPlayer.transform.position - transform.position;
            transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
            m_HorizontalAxis = direction.x;
            m_VerticalAxis = direction.z;
            m_bChargeInput = true;
        }
        else if (m_bChargeInput)
        {
            m_CurrentState = SearchBox;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Waves layer
        if (other.gameObject.layer == 8 && !m_AttempedToEvadeWave)
        {
            m_AttempedToEvadeWave = true;
            int random = Random.Range(0, 101);
            if (random <= m_CurrentProfile.WaveEvasionChance)
                m_bJumpInput = true;
            StartCoroutine(ClearWaveEvadeState());
        }
        //Boxes layer
        else if (other.gameObject.layer == 9)
        {
            int random = Random.Range(0, 101);
            if (random <= m_CurrentProfile.EvasionChance)
            {
                BaseBox bBox = other.GetComponent<BaseBox>();
                if (bBox != m_TargetBox && bBox != m_PickedUpBox)
                {
                    if (bBox.IsThrown && bBox.PlayerOwner != this)
                    {
                        if (m_CurrentState != Evade)
                        {
                            m_TargetBox = bBox;
                            m_CurrentState = Evade;
                        }
                    }
                }
            }
        }
        else if (other.gameObject.layer == gameObject.layer)
        {
            if (other.gameObject != gameObject && m_PickedUpBox == null && m_bCanCharge)
            {
                if (m_CurrentState != Charge && m_CurrentState != Evade)
                {
                    m_TargetPlayer = other.gameObject.GetComponent<PlayerController>();
                    m_CurrentState = Charge;
                }
            }
        }
    }

    public void SetProfile(AIProfile profile)
    {
        m_CurrentProfile = profile;
    }

    IEnumerator ClearWaveEvadeState()
    {
        yield return new WaitForSeconds(3f);
        m_AttempedToEvadeWave = false;
    }
}
