﻿using UnityEngine;

public class AIProfile : ScriptableObject
{
    [Range(0f, 5f)]
    public float WoodBoxPriority;

    [Range(0f, 5f)]
    public float TNTBoxPriority;

    [Range(0, 100)]
    public int EvasionChance; 

    [Range(0, 100)]
    public int AimAccuracy;

    [Range(0, 100)]
    public int WaveEvasionChance;

    public bool CanJumpNitro;
    public bool CanEvadeNitroWhileCarryingBoxes;

    public void SetRandomValues()
    {
        WoodBoxPriority = Random.Range(0f, 5f);
        TNTBoxPriority = Random.Range(0f, 5f);
        EvasionChance = Random.Range(0, 101);
        AimAccuracy = Random.Range(0, 101);
        WaveEvasionChance = Random.Range(0, 101);

        CanJumpNitro = Random.Range(0, 101) > 50;
        CanEvadeNitroWhileCarryingBoxes = Random.Range(0, 101) > 50;
    }
}