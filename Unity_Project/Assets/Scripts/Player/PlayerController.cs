﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerController : AchievementsSubject
{
    // --------------------------------------------------------------

    // The character's running speed
    [SerializeField]
    float m_RunSpeed = 5.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    // The character's jump height
    [SerializeField]
    float m_JumpHeight = 4.0f;

    //Charge's max distance
    [SerializeField]
    float m_ChargeMaxDistance = 1.5f;

    //Knockback's max distance
    [SerializeField]
    float m_KnockbackMaxDistance = 1.5f;

    //Charge's cooldown time
    [SerializeField]
    float m_ChargeCooldown = 1.5f;

    //Stun time
    [SerializeField]
    float m_StunTime = 1.5f;

    //Invulnerable time
    [SerializeField]
    float m_InvulnerableTime = 1f;

    [SerializeField]
    GameObject m_StunEffect = null;

    // --------------------------------------------------------------

    // Identifier for Input
    string m_PlayerInputString = "_P1";

    // The charactercontroller of the player
    protected CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current movement speed
    float m_MovementSpeed = 0.0f;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // Whether the player is alive or not
    bool m_bIsAlive = false;
    public bool IsAlive { get { return m_bIsAlive; } }
    // --------------------------------------------------------------

    protected bool m_bIsAIControlled = false;
    public bool IsAIControlled { get { return m_bIsAIControlled; } }

    protected bool m_bChargeInput = false;
    protected bool m_bJumpInput = false;
    protected bool m_FireInput = false;

    protected float m_HorizontalAxis = 0f;
    protected float m_VerticalAxis = 0f;

    bool m_bPickUpIntention = false;
    bool m_bIsStunned = false;
    bool m_bIsInvulnerable = false;
    protected bool m_bIsCharging = false;
    protected bool m_bCanCharge = true;
    public bool IsInvulnerable { get { return m_bIsInvulnerable; } }

    float m_SpeedModifier = 1f;
    float m_StunStartTime = 0f;
    float m_InvulnerableStartTime = 0f;
    float m_ChargeCooldownStartTime = 0f;

    protected BaseBox m_PickedUpBox = null;
    Vector3 m_ChargeStartPosition = Vector3.zero;
    Vector3 m_KnockbackStartPosition = Vector3.zero;
    Vector3 m_KnockbackDirection = Vector3.zero;
    Material m_PlayerMaterial = null;
    Material m_PlayerEyeMaterial = null;
    AudioManager m_AudioManager = null;

    int m_PlayerHealth = 100;
    int m_PlayerNumber = 0;

    public delegate void PlayerHealthEvent(int playerNum, int playerHealth);
    PlayerHealthEvent OnPlayerHealthChanged;

    public delegate void PlayerDeath(int playerNum);
    PlayerDeath OnPlayerDeath;

    protected virtual void Awake()
    {
        m_MovementSpeed = m_RunSpeed;
        m_AudioManager = AudioManager.Instance;
        m_CharacterController = GetComponent<CharacterController>();
        m_PlayerMaterial = GetComponent<Renderer>().material;
        m_PlayerEyeMaterial = transform.GetChild(0).GetComponent<Renderer>().sharedMaterial;

        GameManager.GameUnPaused += GameUnPaused;
        InitializeSubject(DestructionBehaviour.None);
    }

    private void GameUnPaused(float timeDiff)
    {
        m_InvulnerableStartTime += timeDiff;
        m_ChargeCooldownStartTime += timeDiff;
        m_StunStartTime += timeDiff;
    }

    void Start()
    {
        StartCoroutine(SpawnEffect());
    }

    void Jump()
    {
        m_VerticalSpeed = Mathf.Sqrt(m_JumpHeight * m_Gravity);
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    void UpdateMovementState()
    {
        if (m_PickedUpBox == null && m_bCanCharge && m_bChargeInput)
        {
            m_bChargeInput = false;
            m_bIsCharging = true;
            m_bCanCharge = false;
            m_ChargeCooldownStartTime = Time.time;

            m_SpeedModifier = 5.5f;
            m_ChargeStartPosition = transform.position;

            m_AudioManager.PlayOneShot(AudioClipType.Charge, 0.5f);

            if (m_MovementDirection == Vector3.zero)
                m_MovementDirection = transform.forward;
        }

        // Get Player's movement input and determine direction and set run speed
        if (!m_bIsCharging)
        {
            m_MovementDirection = new Vector3(m_HorizontalAxis, 0, m_VerticalAxis);
            m_MovementSpeed = m_RunSpeed;
        }
    }

    void UpdateJumpState()
    {
        // Character can jump when standing on the ground
        if (m_bJumpInput && m_CharacterController.isGrounded)
        {
            Jump();
            m_bJumpInput = false;
        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (!m_bIsAlive) return;

        if (m_bIsInvulnerable)
        {
            if (Time.time - m_InvulnerableStartTime >= m_InvulnerableTime)
            {

                m_bIsInvulnerable = false;
            }
        }

        if (!m_bCanCharge)
        {
            if (Time.time - m_ChargeCooldownStartTime >= m_ChargeCooldown)
                m_bCanCharge = true;
        }

        if (m_bIsStunned)
        {
            m_CharacterController.Move((m_KnockbackDirection * m_MovementSpeed * 5 + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime);

            if (Vector3.Distance(transform.position, m_KnockbackStartPosition) >= m_KnockbackMaxDistance)
                m_KnockbackDirection = Vector3.zero;

            if (Time.time - m_StunStartTime >= m_StunTime)
            {
                m_StunEffect.SetActive(false);
                m_bIsStunned = false;
            }

            return;
        }

        if (!m_bIsAIControlled)
            UpdateAllInput();

        // Update movement input
        UpdateMovementState();

        // Update jumping input and apply gravity
        UpdateJumpState();
        ApplyGravity();

        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed * m_SpeedModifier + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);

        // Stop After Chraging
        if (m_bIsCharging && Vector3.Distance(m_ChargeStartPosition, transform.position) >= m_ChargeMaxDistance)
        {
            m_bIsCharging = false;
            m_SpeedModifier = 1.0f;
        }

        UpdateThrowState();

        // Rotate the character in movement direction
        if (m_MovementDirection != Vector3.zero && !m_bIsCharging)
        {
            RotateCharacter(m_MovementDirection);
        }
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    public int GetPlayerNum()
    {
        return m_PlayerNumber;
    }

    public string GetPlayerString()
    {
        return m_PlayerInputString;
    }

    public void Die()
    {
        if(m_bIsAlive)
        {
            m_bIsAlive = false;
            if (OnPlayerDeath != null)
                OnPlayerDeath(m_PlayerNumber);

            if(!m_bIsAIControlled)
                UnlockAchievement();
            gameObject.SetActive(false);
        }
    }

    void UpdateThrowState()
    {
        if (m_PickedUpBox == null)
            m_bPickUpIntention = m_FireInput && m_CharacterController.isGrounded;
        else
        {
            if (!m_FireInput)
            {
                m_SpeedModifier = 1f;
                m_PickedUpBox.Throw();
                m_PickedUpBox = null;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            if (m_bPickUpIntention)
            {
                BaseBox bBox = other.GetComponent<BaseBox>();
                if (bBox.IsPickable)
                {
                    m_bPickUpIntention = false;
                    m_PickedUpBox = bBox;
                    m_SpeedModifier = 0.75f;
                    m_PickedUpBox.BindToPlayer(gameObject);
                }
            }
        }
    }

    public void Stun(Vector3 enemyPos)
    {
        if (m_bIsInvulnerable) return;

        m_StunEffect.SetActive(true);
        m_bIsStunned = true;
        m_StunStartTime = Time.time;

        if (m_PickedUpBox != null)
        {
            m_PickedUpBox.Drop();
            m_PickedUpBox = null;
        }

        m_AudioManager.PlayOneShot(AudioClipType.Knockback);
        m_KnockbackStartPosition = transform.position;
        m_KnockbackDirection = (transform.position - enemyPos).normalized;
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (m_bIsCharging)
        {
            // Floor's layer
            if (hit.gameObject.layer != 13 && hit.point.y >= transform.position.y - m_CharacterController.height / 2f)
            {
                m_bIsCharging = false;
                m_SpeedModifier = 1.0f;

                if (hit.gameObject.layer == gameObject.layer)
                {
                    hit.gameObject.GetComponent<PlayerController>().Stun(transform.position);
                }

                if (hit.gameObject.layer == 9)
                {
                    BaseBox bBox = hit.collider.GetComponent<BaseBox>();
                    if (!bBox.IsThrown && bBox.PlayerOwner != null)
                        bBox.PlayerOwner.Stun(transform.position);
                }
            }
        }
        else
        {
            // 9 is the boxes layer
            if (hit.gameObject.layer == 9)
            {
                BaseBox bBox = hit.collider.GetComponent<BaseBox>();
                bBox.TouchedByPlayer(gameObject);
            }
        }
    }

    public void ApplyDamage(int damage)
    {
        if (m_bIsInvulnerable) return;

        if (m_PickedUpBox != null)
        {
            m_PickedUpBox.Drop();
            m_PickedUpBox = null;
        }

        m_PlayerHealth -= damage;
        m_PlayerHealth = Mathf.Max(0, m_PlayerHealth);

        if (OnPlayerHealthChanged != null)
            OnPlayerHealthChanged(m_PlayerNumber, m_PlayerHealth);

        if (m_PlayerHealth == 0)
            Die();
        else
        {
            m_InvulnerableStartTime = Time.time;
            m_bIsInvulnerable = true;
            StartCoroutine(InvulnerableEffect());
        }
    }

    public virtual void SetAlive()
    {
        m_bIsAlive = true;
    }

    IEnumerator InvulnerableEffect()
    {
        Color startColor = m_PlayerMaterial.color;
        Color effectColor = new Color(startColor.r, startColor.g, startColor.b, 0.1f);

        bool lerpValue = true;
        while (m_bIsInvulnerable)
        {
            while (GameManager.GamePaused)
                yield return null;

            m_PlayerMaterial.color = Color.Lerp(startColor, effectColor, lerpValue ? 1 : 0);
            lerpValue = !lerpValue;
            yield return new WaitForSeconds(0.1f);
        }

        m_PlayerMaterial.color = startColor;
    }

    IEnumerator SpawnEffect()
    {
        for (float i = 0; i <= 1; i += 0.01f)
        {
            m_PlayerMaterial.SetFloat("_Cutout", Mathf.Lerp(1, 0, i));
            m_PlayerEyeMaterial.SetFloat("_Cutout", Mathf.Lerp(1, 0, i));
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void SetupPlayerHealthCallback(PlayerHealthEvent healthCallback)
    {
        OnPlayerHealthChanged += healthCallback;
    }

    public void SetupPlayerData(int playerNumber, string playerInput, PlayerDeath deathCallback)
    {
        m_PlayerNumber = playerNumber;
        m_PlayerInputString = playerInput;
        OnPlayerDeath += deathCallback;
    }

    public void CleanUpBox()
    {
        m_PickedUpBox = null;
        m_SpeedModifier = 1f;
    }

    void UpdateAllInput()
    {
        m_bChargeInput = Input.GetButtonDown("Charge" + m_PlayerInputString);
        m_HorizontalAxis = Input.GetAxisRaw("Horizontal" + m_PlayerInputString);
        m_VerticalAxis = Input.GetAxisRaw("Vertical" + m_PlayerInputString);
        m_bJumpInput = Input.GetButtonDown("Jump" + m_PlayerInputString);
        m_FireInput = Input.GetButton("Fire" + m_PlayerInputString);
    }
}