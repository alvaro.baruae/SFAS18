﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct AchievementSpritePair
{
    public Achievement achievement;
    public Sprite sprite;
}

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance { get { return instance; } }

    [SerializeField]
    Text m_MatchCountdown = null;

    [SerializeField]
    Text m_MatchTimer = null;

    [SerializeField]
    Text m_SuddenDeathText = null;

    [SerializeField]
    Text m_WinText = null;

    [SerializeField]
    GameObject m_PauseText = null;

    [SerializeField]
    RectTransform m_AchievementPanel = null;

    Text m_AchievementName = null;
    Image m_AchievementImage = null;
    bool m_bIsShowingAchievementPanel = false;
    Queue<Achievement> m_AchievementQueue = new Queue<Achievement>();

    [HideInInspector] public AchievementSpritePair[] m_AchievementSpritePairList;
    Dictionary<Achievement, Sprite> m_AchievementSpriteDict = new Dictionary<Achievement, Sprite>();

    [SerializeField]
    RawImage[] m_HealthBars = new RawImage[4];

    void Awake()
    {
        if (instance != null)
            Destroy(this);
        else
            instance = this;

        foreach (AchievementSpritePair pair in m_AchievementSpritePairList)
        {
#if UNITY_EDITOR
            if (m_AchievementSpriteDict.ContainsKey(pair.achievement))
                Debug.LogError(pair.achievement.ToString() + " found more than one time on ui manager sprite list, only first instance added.");
            else
#endif
                m_AchievementSpriteDict.Add(pair.achievement, pair.sprite);
        }

        m_AchievementImage = m_AchievementPanel.GetChild(0).GetComponent<Image>();
        m_AchievementName = m_AchievementPanel.GetChild(1).GetComponent<Text>();
    }

    void OnEnable()
    {
        DeathTrigger.OnPlayerDeath += OnUpdatePlayerStatusToDeath;
    }

    void OnDisable()
    {
        DeathTrigger.OnPlayerDeath -= OnUpdatePlayerStatusToDeath;
    }

    void Update()
    {
        if (!m_bIsShowingAchievementPanel)
        {
            if (m_AchievementQueue.Count > 0)
            {
                DisplayUnlockedAchievement(m_AchievementQueue.Dequeue());
            }
        }
    }

    void OnUpdatePlayerStatusToDeath(int playerNum)
    {
        m_HealthBars[playerNum].rectTransform.sizeDelta = new Vector2(0f, m_HealthBars[playerNum].rectTransform.sizeDelta.y);
    }

    public void UpdateMatchCountdown(int second)
    {
        if (second == 0)
            m_MatchCountdown.gameObject.SetActive(false);
        else
            m_MatchCountdown.text = second.ToString();
    }

    public void UpdateMatchTimer(int minutes, int seconds)
    {
        m_MatchTimer.text = minutes.ToString().PadLeft(2, '0') + ":" + seconds.ToString().PadLeft(2, '0');
    }

    public void DisplaySuddenDeathText()
    {
        m_SuddenDeathText.enabled = true;
    }

    public void SetupHealthBar(PlayerController pController, Color barColor)
    {
        pController.SetupPlayerHealthCallback(OnPlayerHealthChanged);
        int playerNum = pController.GetPlayerNum();

        m_HealthBars[playerNum].color = barColor;
        OnPlayerHealthChanged(playerNum, 100);
    }

    public void OnPlayerHealthChanged(int playerNum, int health)
    {
        m_HealthBars[playerNum].rectTransform.sizeDelta = new Vector2(Mathf.Lerp(0f, 350f, health / 100f), m_HealthBars[playerNum].rectTransform.sizeDelta.y);
    }

    public void SetWinText(int playerNum)
    {
        if (playerNum == -1)
            m_WinText.text = "DRAW!";
        else
        {
            int numToShow = playerNum + 1;
            m_WinText.text = m_WinText.text.Replace("%", numToShow.ToString());
            m_WinText.color = m_HealthBars[playerNum].color;
        }

        m_WinText.gameObject.SetActive(true);
    }

    public void ShowPause()
    {
        m_PauseText.SetActive(true);
    }

    public void HidePause()
    {
        m_PauseText.SetActive(false);
    }

    public void DisplayUnlockedAchievement(Achievement achievement)
    {
        if (!m_bIsShowingAchievementPanel)
        {
            m_AchievementImage.sprite = m_AchievementSpriteDict[achievement];
            m_AchievementName.text = achievement.ToString().Replace('_', ' ');
            StartCoroutine(ShowAchievementPanel());
        }
        else
            m_AchievementQueue.Enqueue(achievement);
    }

    private IEnumerator ShowAchievementPanel()
    {
        m_bIsShowingAchievementPanel = true;

        for (int i = 0; i < 50; ++i)
        {
            m_AchievementPanel.SetLocalPositionY(m_AchievementPanel.localPosition.y + 4);
            yield return new WaitForSeconds(0.01f);
        }

        yield return new WaitForSeconds(3f);

        for (int i = 0; i < 50; ++i)
        {
            m_AchievementPanel.SetLocalPositionY(m_AchievementPanel.localPosition.y - 4);
            yield return new WaitForSeconds(0.01f);
        }

        yield return new WaitForSeconds(1f);
        m_bIsShowingAchievementPanel = false;
    }
}
