﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

    [SerializeField]
    Text[] m_StatusTexts = new Text[4];

    [SerializeField]
    AudioClip m_JoinSFX = null;

    int m_PlayersJoined = 0;
    bool m_bIsWaitingForStart = false;
    List<string> m_PlayerStrings = new List<string>();
    List<int> m_AddedIndices = new List<int>();
    AudioSource m_Music = null;
    MatchData m_MatchData = null;

    private void Awake()
    {
        m_PlayerStrings.Add("_K1");
        m_PlayerStrings.Add("_K2");
        m_PlayerStrings.Add("_J1");
        m_PlayerStrings.Add("_J2");
        m_PlayerStrings.Add("_J3");
        m_PlayerStrings.Add("_J4");

        m_Music = GetComponent<AudioSource>();
        m_MatchData = MatchData.Instance;
    }

    void Start ()
    {
        m_PlayersJoined = 0;
        m_AddedIndices.Clear();
    }
	
	void Update ()
    {
        if (m_bIsWaitingForStart) return;

        for (int i = 0; i < 6; ++i)
        {
            if(!m_AddedIndices.Contains(i))
            {
                if(Input.GetButtonDown("Jump" + m_PlayerStrings[i]))
                {
                    AudioSource.PlayClipAtPoint(m_JoinSFX, Camera.main.transform.position);
                    m_MatchData.playerInputStrings[m_PlayersJoined] = m_PlayerStrings[i];
                    m_AddedIndices.Add(i);

                    SetStatusString(m_PlayersJoined, m_PlayerStrings[i]);
                    ++m_PlayersJoined;
                }
            }
        }
	}

    public void StartGame()
    {
        m_bIsWaitingForStart = true;
        m_MatchData.humansCount = m_PlayersJoined;
        StartCoroutine(WaitToStart());
    }

    public void Exit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    IEnumerator WaitToStart()
    {
        for(int i = 0; i < 20; ++i)
        {
            m_Music.volume -= 0.05f;
            yield return new WaitForSeconds(0.05f);
        }

        SceneManager.LoadScene(1);
    }

    void SetStatusString(int playerNum, string inputString)
    {
        string statusText = string.Empty;

        if (inputString == "_K1")
            statusText = "Joined using Keyboard 1.";
        else if (inputString == "_K2")
            statusText = "Joined using Keyboard 2.";
        else if (inputString == "_J1")
            statusText = "Joined using Controller 1.";
        else if (inputString == "_J2")
            statusText = "Joined using Controller 2.";
        else if (inputString == "_J3")
            statusText = "Joined using Controller 3.";
        else if (inputString == "_J4")
            statusText = "Joined using Controller 4.";
        m_StatusTexts[playerNum].text = statusText;
    }
}
