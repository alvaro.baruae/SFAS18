﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static Vector2 OnUnitCircle()
    {
        float angle = Random.Range(0f, Mathf.PI * 2);
        return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
    }

    public static void SetLocalPositionY(this Transform transform, float newY)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, newY, transform.localPosition.z);
    }

    public static void SetPositionY(this Transform transform, float newY)
    {
        transform.position = new Vector3(transform.position.x, newY, transform.position.z);
    }
}
