﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioClipType
{
    Charge,
    Knockback,
    WoodBoxBreak,
    GroundPounderHitGround,
    Countdown,
    Explosion,
    UI_Click
}

[System.Serializable]
public struct AudioTypePair
{
    public AudioClipType type;
    public AudioClip clip;
}

public class AudioManager : MonoBehaviour
{

    private static AudioManager instance = null;
    public static AudioManager Instance { get { return instance; } }

    AudioSource m_MusicSource = null;

    [SerializeField]
    AudioTypePair[] m_AudioTypePairList;

    Dictionary<AudioClipType, AudioClip> m_AudioDictionary = new Dictionary<AudioClipType, AudioClip>();

    void Awake()
    {
        if (instance != null)
            Destroy(this);
        instance = this;

        m_MusicSource = GetComponent<AudioSource>();
        foreach (AudioTypePair pair in m_AudioTypePairList)
        {
#if UNITY_EDITOR
            if (m_AudioDictionary.ContainsKey(pair.type))
                Debug.LogError(pair.type.ToString() + " found more than one time on audio manager clip list, only first instance added.");
            else
#endif
                m_AudioDictionary.Add(pair.type, pair.clip);
        }
    }

    public void SetSuddenDeath()
    {
        StartCoroutine(ChangeAudioPitch());
    }

    IEnumerator ChangeAudioPitch()
    {
        float accum = 0f;
        for (int i = 0; i < 20; ++i)
        {
            accum += 0.05f;
            m_MusicSource.pitch = Mathf.Lerp(0.75f, 1f, accum);
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void PlayOneShot(AudioClipType type, float volume = 1f, float pitch = 1f)
    {
        CreateOneShotSoundObject(m_AudioDictionary[type], volume, pitch);
    }

    void CreateOneShotSoundObject(AudioClip clip, float volume, float pitch)
    {
        GameObject soundObj = new GameObject("Sound Object");
        soundObj.transform.position = Camera.main.transform.position;

        AudioSource audioSource = soundObj.AddComponent<AudioSource>();
        audioSource.pitch = pitch;

        audioSource.PlayOneShot(clip, volume);

        soundObj.AddComponent<DestroyOnAudioEnd>();
    }
}
