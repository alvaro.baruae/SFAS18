﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    [SerializeField]
    uint m_PlatformSize;

    int m_AnimatingCubeGroups = 0;
    int m_CompletedAnimatingCubeGroups = 0;
    bool m_bPlatformAnimating = false;
    bool m_MatchEnded = false;

    Transform m_GroundPounderTransform;
    List<BoxGroup> m_CubeGroups = new List<BoxGroup>();
    Transform m_FloorParent;
    Transform m_WallsParent;
    BoxCollider m_FloorCollider;

    public delegate void FloorSpawnFinished();
    public event FloorSpawnFinished OnFloorSpawnFinished;
    public event FloorSpawnFinished OnFloorOnPosition;

    void Awake()
    {
        m_PlatformSize *= 2;
        m_GroundPounderTransform = transform.GetChild(1);
        m_FloorParent = transform.GetChild(2);
        m_WallsParent = transform.GetChild(3);

        m_FloorCollider = m_FloorParent.gameObject.AddComponent<BoxCollider>();
        m_FloorCollider.gameObject.layer = 13; // Floor's layer
        m_FloorCollider.size = new Vector3(m_PlatformSize, 1f, m_PlatformSize);
    }

    void Update()
    {
        if (m_bPlatformAnimating)
        {
            if (m_CompletedAnimatingCubeGroups > 0 && m_CompletedAnimatingCubeGroups == m_AnimatingCubeGroups)
            {
                m_GroundPounderTransform.SetLocalPositionY(m_GroundPounderTransform.localPosition.y - 0.2f);
                if (m_GroundPounderTransform.localPosition.y <= 2.5f)
                {
                    m_GroundPounderTransform.localPosition = new Vector3(0f, 2.5f, 0f);
                    m_bPlatformAnimating = false;
                }
                return;
            }

            for (int i = m_CompletedAnimatingCubeGroups; i < m_AnimatingCubeGroups; ++i)
            {
                int k = (m_CubeGroups.Count - 1) - i;
                m_CubeGroups[k].Parent.SetLocalPositionY(m_CubeGroups[k].Parent.localPosition.y - 0.2f);
                if (m_CubeGroups[k].Parent.localPosition.y <= -15f)
                {
                    m_CubeGroups[k].Parent.localPosition = new Vector3(0f, -15f, 0f);
                    ++m_CompletedAnimatingCubeGroups;

                    if (m_CompletedAnimatingCubeGroups == m_AnimatingCubeGroups)
                    {
                        if (OnFloorOnPosition != null)
                            OnFloorOnPosition();
                    }
                }
            }
        }
    }

    public void SpawnFloor()
    {
        for (uint k = 2; k <= m_PlatformSize; k += 2)
        {
            BoxGroup group = new BoxGroup(BoxType.Floor, m_FloorParent);
            for (uint i = 0; i < k; ++i)
            {
                for (uint j = 0; j < k; ++j)
                {
                    if ((i == 0 || i == k - 1) || (j == 0 || j == k - 1))
                    {
                        GameObject cube = BoxFactory.CreateBox(BoxType.Floor);
                        cube.transform.position = new Vector3(-0.5f * (k - 1) + j, 15f, 0.5f * (k - 1) - i);
                        group.AddBox(cube);
                    }
                }
            }

            m_CubeGroups.Add(group);
        }

        //Add Walls
        {
            BoxGroup group = new BoxGroup(BoxType.Wall, m_WallsParent);
            uint k = m_PlatformSize + 2;
            for (uint i = 0; i < k; ++i)
            {
                for (uint j = 0; j < k; ++j)
                {
                    if ((i == 0 || i == k - 1) || (j == 0 || j == k - 1))
                    {
                        GameObject cube = BoxFactory.CreateBox(BoxType.Wall);
                        cube.transform.position = new Vector3(-0.5f * (k - 1) + j, 16f, 0.5f * (k - 1) - i);
                        group.AddBox(cube);
                    }
                }
            }

            m_CubeGroups.Add(group);

            //Objects Collision
            BoxCollider collider;
            float colliderOffset = (k / 2f) - 0.5f;

            collider = m_WallsParent.gameObject.AddComponent<BoxCollider>();
            collider.size = new Vector3(k, 2f, 1f);
            collider.center = new Vector3(0f, 1f, colliderOffset);

            collider = m_WallsParent.gameObject.AddComponent<BoxCollider>();
            collider.size = new Vector3(k, 2f, 1f);
            collider.center = new Vector3(0f, 1f, -colliderOffset);

            collider = m_WallsParent.gameObject.AddComponent<BoxCollider>();
            collider.size = new Vector3(1f, 2f, k);
            collider.center = new Vector3(colliderOffset, 1f, 0f);

            collider = m_WallsParent.gameObject.AddComponent<BoxCollider>();
            collider.size = new Vector3(1f, 2f, k);
            collider.center = new Vector3(-colliderOffset, 1f, 0f);
        }

        if (OnFloorSpawnFinished != null)
            OnFloorSpawnFinished();

        m_bPlatformAnimating = true;
        StartCoroutine(AddGroupsToAnimation());
    }

    IEnumerator AddGroupsToAnimation()
    {
        float interval = 1.5f / 13f;
        for (int i = 0; i < 13; ++i)
        {
            yield return new WaitForSeconds(interval);
            ++m_AnimatingCubeGroups;
        }
    }

    public void StartSuddenDeathCoroutine()
    {
        StartCoroutine(SuddenDeath());
    }

    public void StopSuddenDeathCoroutine()
    {
        m_MatchEnded = true;
    }

    public IEnumerator SuddenDeath()
    {
        BoxCollider[] wallColliders = m_WallsParent.GetComponents<BoxCollider>();
        foreach (BoxCollider bCol in wallColliders)
            bCol.enabled = false;

        for (int i = 12; i >= 3; --i)
        {
            for (int j = 0; j < 180; ++j)
            {
                if (m_MatchEnded)
                    break;

                while (GameManager.GamePaused)
                    yield return null;

                m_CubeGroups[i].Parent.SetPositionY(m_CubeGroups[i].Parent.position.y - 0.1f);
                yield return new WaitForSeconds(0.01f);
            }

            if (m_MatchEnded)
                break;

            if (i != 3)
            {
                m_PlatformSize -= 2;
                m_FloorCollider.size = new Vector3(m_PlatformSize, 1f, m_PlatformSize);
            }
        }
    }
}
