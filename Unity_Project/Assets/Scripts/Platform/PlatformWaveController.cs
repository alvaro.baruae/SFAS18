﻿using System.Collections;
using UnityEngine;

public class PlatformWaveController : MonoBehaviour
{
    // --------------------------------------------------------------

    [Header("Wave Properties")]

    // The object material
    [SerializeField]
    Material m_WaveMaterial;

    // The waves's speed in seconds
    [SerializeField]
    float m_WaveSpeed = 5.0f;

    [SerializeField]
    float m_WaveFrequency = 5f;

    // The wave's maximun spread across the playfield
    [SerializeField]
    float m_WaveMaximunDisplacement = 5.0f;

    // The wave's initial radius to accomodate for the central pillar
    [SerializeField]
    float m_WaveInitialRadius = 5.0f;

    [Header("Pillar Properties")]
    // The pillar's upwards movement speed in seconds
    [SerializeField]
    float m_PillarUpMovementSpeed = 1.5f;

    // The pillar's downwards movement speed in seconds
    [SerializeField]
    float m_PillarDownMovementSpeed = 0.5f;

    // The pillar's maximun Y position
    [SerializeField]
    float m_PillarMaxYPosition = 0.5f;

    // The pillar's minumum Y position
    [SerializeField]
    float m_PillarMinYPosition = 0.5f;

    // --------------------------------------------------------------

    // Is the wave currently animating
    bool m_bWaveAnimating = false;

    // The wave's animation start time
    float m_WaveMovementStartTime = 0.0f;

    // The wave's collider
    PlatformWaveCollider m_WaveCollider;

    // The pillar transform
    Transform m_PillarTransform;

    // The pillar animation start time
    float m_PillarMovementStartTime = 0.0f;

    // Is the pillar moving upwards
    bool m_bPillarMovingUp = false;

    // Is the pillar moving
    bool m_bPillarAnimating = false;

    // The pillar's initial Y position
    float m_PillarInitialYPos = 0f;

    AudioManager m_AudioManager = null;

    void Awake()
    {
        m_AudioManager = AudioManager.Instance;
        m_WaveCollider = transform.GetChild(0).GetComponent<PlatformWaveCollider>();
        m_WaveMaximunDisplacement -= m_WaveInitialRadius;
        m_PillarTransform = transform.GetChild(1);
        m_PillarInitialYPos = 2.5f;

        GameManager.GameUnPaused += GameUnPaused;
    }

    private void GameUnPaused(float timeDiff)
    {
        m_PillarMovementStartTime += timeDiff;
        m_WaveMovementStartTime += timeDiff;
    }

    void Update()
    {
        if (m_bPillarAnimating && !GameManager.GamePaused)
        {
            float speed = m_PillarUpMovementSpeed;
            float yOrig = m_PillarInitialYPos;
            float yDest = m_PillarMaxYPosition;

            if (!m_bPillarMovingUp)
            {
                speed = m_PillarDownMovementSpeed;
                yDest = m_PillarMinYPosition;
                yOrig = m_PillarMaxYPosition;
            }

            float animationDelta = (Time.time - m_PillarMovementStartTime) / speed;
            if (animationDelta > 1f)
            {
                animationDelta = 1f;
                if (m_bPillarMovingUp)
                {
                    m_PillarMovementStartTime = Time.time;
                    m_bPillarMovingUp = false;
                }
                else
                {
                    m_AudioManager.PlayOneShot(AudioClipType.GroundPounderHitGround, 3);
                    StartWaveMovement();
                    m_bPillarAnimating = false;
                }
            }

            m_PillarTransform.localPosition = new Vector3(0f, Mathf.Lerp(yOrig, yDest, animationDelta), 0f);
        }

        if (m_bWaveAnimating && !GameManager.GamePaused)
        {
            float animationDelta = (Time.time - m_WaveMovementStartTime) / m_WaveSpeed;
            if (animationDelta > 1f)
            {
                m_bWaveAnimating = false;
                animationDelta = 1f;
            }

            float wavesCurrentRadius = m_WaveInitialRadius + (animationDelta * m_WaveMaximunDisplacement);
            m_WaveCollider.UpdateCollisionShape(wavesCurrentRadius);
            m_WaveMaterial.SetFloat("_WaveDistance", wavesCurrentRadius);
            m_PillarTransform.localPosition = new Vector3(0f, Mathf.Lerp(m_PillarMinYPosition, m_PillarInitialYPos, animationDelta), 0f);
        }
    }

    public void StartWaveMovement()
    {
        m_WaveMovementStartTime = Time.time;
        m_WaveMaterial.SetFloat("_WaveDistance", m_WaveInitialRadius);
        m_WaveCollider.UpdateCollisionShape(m_WaveInitialRadius);
        m_bWaveAnimating = true;
    }

    public void StartGroundPound()
    {
        StartCoroutine(WaveLoop());
    }

    public void StartSuddenDeath()
    {
        m_WaveFrequency /= 2f;
        m_PillarDownMovementSpeed /= 2f;
        m_PillarUpMovementSpeed /= 2f;
    }

    IEnumerator WaveLoop()
    {
        while(true)
        {
            m_PillarMovementStartTime = Time.time;
            m_bPillarAnimating = true;
            m_bPillarMovingUp = true;

            yield return new WaitForSeconds(m_WaveFrequency);
        }
    }
}
