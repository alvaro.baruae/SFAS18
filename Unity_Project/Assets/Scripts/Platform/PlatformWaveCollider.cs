﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformWaveCollider : MonoBehaviour
{
    // --------------------------------------------------------------

    // The collider current radius
    [SerializeField]
    float m_ColliderMaximumRadius = 2f;

    [SerializeField]
    int m_ColliderSpheresCount = 60;

    // --------------------------------------------------------------
    
    LayerMask m_CollisionLayer;
    Transform colliderParent;
    GameObject waveEffector;
    List<GameObject> m_CollisionSpheres = new List<GameObject>();

    int m_Step = 0;
    float m_ColliderRadius = 2f;

    void Awake()
    {
        m_CollisionLayer = LayerMask.NameToLayer("Wave");

        colliderParent = new GameObject("Colliders Parent").transform;
        colliderParent.gameObject.SetActive(false);
        colliderParent.parent = transform;

        waveEffector = new GameObject("WaveEffector");
        waveEffector.layer = 12;
        waveEffector.AddComponent<SphereCollider>().isTrigger = true;
        waveEffector.SetActive(false);
        waveEffector.transform.parent = transform;

        CreateCollisionShape();
    }

    void CreateCollisionShape()
    {
        m_Step = 360 / m_ColliderSpheresCount;

        for (int i = 0; i < 360; i += m_Step)
        {
            GameObject sphere = new GameObject("Sphere Collision Shape");
            sphere.AddComponent<SphereCollider>();

            float xPos = Mathf.Cos(i * Mathf.Deg2Rad) * m_ColliderRadius;
            float zPos = Mathf.Sin(i * Mathf.Deg2Rad) * m_ColliderRadius;

            sphere.layer = m_CollisionLayer;
            sphere.transform.position = new Vector3(xPos, 1f, zPos);
            sphere.transform.parent = colliderParent;
            m_CollisionSpheres.Add(sphere);
        }
    }

    public void UpdateCollisionShape(float newCollisionRadius)
    {
        if (newCollisionRadius >= m_ColliderMaximumRadius)
        {
            waveEffector.SetActive(false);
            colliderParent.gameObject.SetActive(false);
            return;
        }

        if (!colliderParent.gameObject.activeSelf)
        {
            waveEffector.SetActive(true);
            colliderParent.gameObject.SetActive(true);
        }

        m_ColliderRadius = newCollisionRadius;

        float effectorSize = newCollisionRadius * 2;
        waveEffector.transform.localScale = new Vector3(effectorSize, effectorSize, effectorSize);

        for (int i = 0; i < m_CollisionSpheres.Count; ++i)
        {
            float xPos = Mathf.Cos((i * m_Step) * Mathf.Deg2Rad) * m_ColliderRadius;
            float zPos = Mathf.Sin((i * m_Step) * Mathf.Deg2Rad) * m_ColliderRadius;
            m_CollisionSpheres[i].transform.position = new Vector3(xPos, 1.07999f, zPos);
        }
    }
}
