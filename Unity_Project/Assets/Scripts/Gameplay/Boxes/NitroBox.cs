﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NitroBox : BaseBox
{
    static GameObject m_ExplosionPrefab = null;
    private bool m_HasExploded = false;

    void Start()
    {
        if (m_ExplosionPrefab == null)
        {
            m_ExplosionPrefab = Resources.Load<GameObject>("Prefabs/NitroExplosion");
        }

        m_Damage = 20;
        OnTouchedFloorAfterEffector += TouchExplode;
        OnPlayerHitAfterEffector += PlayerHit;
        OnTouchedByPlayer += PlayerHit;
        OnBoxHitAfterThrow += TouchExplode;
    }

    private void PlayerHit(GameObject player)
    {
        Explode();
    }

    private void TouchExplode()
    {
        Explode();
    }

    public override void HitByExplosion()
    {
        Explode();
    }

    void Explode()
    {
        if (m_HasExploded) return;

        m_HasExploded = true;
        Instantiate(m_ExplosionPrefab, transform.position, Quaternion.identity);

        Collider[] players = Physics.OverlapSphere(transform.position, 2f, m_PlayerMask);
        foreach(Collider col in players)
            col.GetComponent<PlayerController>().ApplyDamage(m_Damage);

        Collider[] boxes = Physics.OverlapSphere(transform.position, 2f, m_BoxMask);
        foreach (Collider col in boxes)
            if(col.gameObject != gameObject)
                col.GetComponent<BaseBox>().HitByExplosion();

        DestroyBox();
    }
}
