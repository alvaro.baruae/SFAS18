﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum BoxType
{
    Floor,
    Wall,
    Wood,
    TNT,
    Nitro
}

public class BoxGroup
{
    public BoxType Type { get; private set; }
    public Transform Parent { get; private set; }

    private List<GameObject> m_BoxList;

    public BoxGroup(BoxType type, Transform parent)
    {
        Type = type;

        GameObject realParent = new GameObject("CubeGroup");
        realParent.transform.parent = parent;
        Parent = realParent.transform;

        m_BoxList = new List<GameObject>();
    }

    public void AddBox(GameObject cube)
    {
        m_BoxList.Add(cube);
        cube.transform.parent = Parent;
    }

    public void DestroyAllBoxes()
    {
        foreach (GameObject obj in m_BoxList)
            UnityEngine.Object.Destroy(obj);
    }
}
