﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBox : MonoBehaviour
{
    static protected LayerMask m_BoxLayer = -1;
    static protected LayerMask m_PlayerLayer = -1;
    static protected LayerMask m_FloorLayer = -1;
    static protected LayerMask m_WaveEffectorLayer = -1;
    static protected LayerMask m_WallLayer = -1;
    static protected int m_PlayerMask = -1;
    static protected int m_BoxMask = -1;
    static protected int m_FloorMask = -1;

    protected int m_Damage;
    protected BoxType m_BoxType;
    protected Rigidbody m_RigidBody;
    protected BoxCollider m_BoxCollider;
    protected PlayerController m_PlayerOwner;
    protected Material m_BoxMaterial = null;
    protected bool m_bIsOnAirByEffector = false;
    protected bool m_bIsBindedToPlayer = false;
    protected bool m_bThrownByPlayer = false;
    protected AudioManager m_AudioManager = null;

    protected delegate void BoxEvent();
    protected delegate void BoxPlayerEvent(GameObject player);

    protected event BoxEvent OnTouchedFloorAfterEffector;
    protected event BoxEvent OnTouchedFloorAfterThrown;
    protected event BoxEvent OnBindedToPlayer;
    protected event BoxEvent OnTouchedWall;
    protected event BoxEvent OnBoxHitAfterThrow;

    protected event BoxPlayerEvent OnTouchedByPlayer;
    protected event BoxPlayerEvent OnPlayerHitAfterThrow;
    protected event BoxPlayerEvent OnPlayerHitAfterEffector;

    public BoxType BoxType { get { return m_BoxType; } }
    public bool IsPickable { get { return !m_bIsBindedToPlayer && !m_bThrownByPlayer; } }
    public bool IsThrown { get { return m_bThrownByPlayer; } }
    public PlayerController PlayerOwner { get { return m_PlayerOwner; } }

    void Awake()
    {
        GameObject.FindGameObjectsWithTag("Player");
        if (m_BoxLayer == -1)
        {
            m_PlayerLayer = LayerMask.NameToLayer("Player");
            m_PlayerMask = 1 << m_PlayerLayer;

            m_BoxLayer = LayerMask.NameToLayer("Boxes");
            m_BoxMask = 1 << m_BoxLayer;

            m_FloorLayer = LayerMask.NameToLayer("Floor");
            m_FloorMask = 1 << m_FloorLayer;

            m_WaveEffectorLayer = LayerMask.NameToLayer("WaveEffector");
            m_WallLayer = LayerMask.NameToLayer("Wall");
        }

        m_RigidBody = GetComponent<Rigidbody>();
        m_RigidBody.sleepThreshold = 0;
        m_BoxMaterial = GetComponent<Renderer>().material;
        m_AudioManager = AudioManager.Instance;
        gameObject.layer = m_BoxLayer;

        StartCoroutine(SpawnEffect());
    }

    protected void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == m_FloorLayer)
        {
            if (m_bIsOnAirByEffector)
            {
                m_bIsOnAirByEffector = false;
                if (OnTouchedFloorAfterEffector != null)
                    OnTouchedFloorAfterEffector();
            }

            if (m_bThrownByPlayer)
            {
                if (OnTouchedFloorAfterThrown != null)
                    OnTouchedFloorAfterThrown();
            }

            return;
        }

        if (col.gameObject.layer == m_PlayerLayer)
        {
            if (m_bIsOnAirByEffector && m_RigidBody.velocity.y < 0)
            {
                if (OnPlayerHitAfterEffector != null)
                    OnPlayerHitAfterEffector(col.gameObject);
                return;
            }

            if (m_bThrownByPlayer)
            {
                if (OnPlayerHitAfterThrow != null)
                    OnPlayerHitAfterThrow(col.gameObject);
                return;
            }

            return;
        }

        if (col.gameObject.layer == m_BoxLayer)
        {
            if (m_bThrownByPlayer)
            {
                BaseBox bBox = col.gameObject.GetComponent<BaseBox>();
                if (!bBox.IsThrown && bBox.PlayerOwner != null)
                {
                    if (OnPlayerHitAfterThrow != null)
                        OnPlayerHitAfterThrow(bBox.PlayerOwner.gameObject);
                }
                else if (OnBoxHitAfterThrow != null)
                {
                    if (bBox.OnBoxHitAfterThrow != null)
                        bBox.OnBoxHitAfterThrow();
                    OnBoxHitAfterThrow();
                }
                return;
            }
        }

        if (col.gameObject.layer == m_WallLayer)
        {
            if (OnTouchedWall != null)
                OnTouchedWall();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        //Wave's effector layer
        if (!m_bIsBindedToPlayer && !m_bThrownByPlayer && col.gameObject.layer == m_WaveEffectorLayer)
        {
            m_bIsOnAirByEffector = true;
            m_RigidBody.AddForce(Vector3.up * 10f, ForceMode.Impulse);
        }
    }

    public void TouchedByPlayer(GameObject player)
    {
        if (OnTouchedByPlayer != null)
            OnTouchedByPlayer(player);
    }

    public void BindToPlayer(GameObject player)
    {
        m_bIsBindedToPlayer = true;
        Destroy(m_RigidBody);

        m_PlayerOwner = player.GetComponent<PlayerController>();
        transform.parent = player.transform;
        transform.localPosition = new Vector3(0, -0.2f, 1.2f);
        transform.localRotation = Quaternion.identity;

        if (OnBindedToPlayer != null)
            OnBindedToPlayer();
    }

    public void Throw()
    {
        m_bIsBindedToPlayer = false;
        m_bThrownByPlayer = true;
        m_RigidBody = gameObject.AddComponent<Rigidbody>();
        m_BoxCollider.enabled = false;
        m_RigidBody.mass = 2;

        transform.parent = null;
        m_RigidBody.AddForce((transform.forward + (Vector3.up * 0.5f)) * 15f, ForceMode.Impulse);
    }

    public void Drop()
    {
        m_PlayerOwner = null;
        m_bIsBindedToPlayer = false;
        if(m_RigidBody == null)
            m_RigidBody = gameObject.AddComponent<Rigidbody>();
        m_RigidBody.mass = 2;

        transform.parent = null;
    }

    protected void DestroyBox()
    {
        switch (m_BoxType)
        {
            case BoxType.Wood:
                m_AudioManager.PlayOneShot(AudioClipType.WoodBoxBreak);

                break;
            case BoxType.TNT:
            case BoxType.Nitro:
                m_AudioManager.PlayOneShot(AudioClipType.WoodBoxBreak);
                m_AudioManager.PlayOneShot(AudioClipType.Explosion, 0.3f);
                break;
            default:
                break;
        }
        Destroy(gameObject);
    }

    public void SetUpBox(BoxType bType)
    {
        m_BoxType = bType;
        m_BoxCollider = GetComponent<BoxCollider>();

        if (m_BoxType != BoxType.Nitro)
            gameObject.AddComponent<BoxCollider>();
    }

    public virtual void HitByExplosion()
    {
        DestroyBox();
    }

    IEnumerator SpawnEffect()
    {
        for (float i = 0; i <= 1; i += 0.2f)
        {
            while (GameManager.GamePaused)
                yield return null;

            m_BoxMaterial.SetFloat("_Cutout", Mathf.Lerp(1, 0, i));
            yield return new WaitForSeconds(0.1f);
        }
    }
}
