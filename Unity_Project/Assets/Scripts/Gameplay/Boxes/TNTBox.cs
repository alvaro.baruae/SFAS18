﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TNTBox : BaseBox
{
    static GameObject m_ExplosionPrefab = null;

    bool m_HasExploded = false;

    Color m_InitialColor = Color.black;

    bool m_bCountdownStarted = false;
    float m_CountdownStartTime = 0f;

    void Start()
    {
        if(m_ExplosionPrefab == null)
        {
            m_ExplosionPrefab = Resources.Load<GameObject>("Prefabs/TNTExplosion");
        }

        m_Damage = 15;
        m_InitialColor = m_BoxMaterial.color;

        OnPlayerHitAfterThrow += PlayerTouchExplode;
        OnPlayerHitAfterEffector += PlayerTouchExplode;
        OnBindedToPlayer += TNTBox_OnBindedToPlayer;
        OnTouchedFloorAfterThrown += TouchExplode;
        OnBoxHitAfterThrow += TouchExplode;
        OnTouchedWall += TouchExplode;

        GameManager.GameUnPaused += GameUnPaused;
    }

    private void GameUnPaused(float timeDiff)
    {
        m_CountdownStartTime += timeDiff;
    }

    void Update()
    {
        if(m_bCountdownStarted)
        {
            float timeDiff = Time.time - m_CountdownStartTime;
            
            if (timeDiff >= 3f)
            {
                if(m_PlayerOwner != null)
                    m_PlayerOwner.CleanUpBox();
                Explode();
            }
        }
    }

    private void TNTBox_OnBindedToPlayer()
    {
        m_CountdownStartTime = Time.time;
        m_bCountdownStarted = true;
        StartCoroutine(ExplosionWarningEffect());
    }

    private void TouchExplode()
    {
        Explode();
    }

    private void PlayerTouchExplode(GameObject player)
    {
        Explode();
    }

    public override void HitByExplosion()
    {
        Explode();
    }

    void Explode()
    {
        if (m_HasExploded) return;

        m_HasExploded = true;
        Instantiate(m_ExplosionPrefab, transform.position, Quaternion.identity);

        Collider[] players = Physics.OverlapSphere(transform.position, 2f, m_PlayerMask);
        foreach (Collider col in players)
            col.GetComponent<PlayerController>().ApplyDamage(m_Damage);

        Collider[] boxes = Physics.OverlapSphere(transform.position, 2f, m_BoxMask);
        foreach (Collider col in boxes)
            if (col.gameObject != gameObject)
                col.GetComponent<BaseBox>().HitByExplosion();

        DestroyBox();
    }

    IEnumerator ExplosionWarningEffect()
    {
        float step = 1f / 50f;

        for(int k = 0; k < 3; ++k)
        {
            for(int i = 0; i < 50; ++i)
            {
                while (GameManager.GamePaused)
                    yield return null;

                m_BoxMaterial.color = Color.Lerp(m_InitialColor, Color.white, k % 2 == 0 ? step * i : 1 - (step * i));
                yield return new WaitForSeconds(0.01f);
            }
        }

        for (int i = 0; i < 50; ++i)
        {
            while (GameManager.GamePaused)
                yield return null;

            m_BoxMaterial.color = Color.Lerp(Color.white, Color.black, step * i);
            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.5f, step * i);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
