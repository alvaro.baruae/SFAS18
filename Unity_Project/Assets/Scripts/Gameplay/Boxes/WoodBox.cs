﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodBox : BaseBox
{
    void Start()
    {
        m_Damage = 10;
        OnPlayerHitAfterThrow += PlayerHit;
        OnPlayerHitAfterEffector += PlayerHit;
        OnTouchedFloorAfterThrown += TouchDestroy;
        OnTouchedWall += TouchDestroy;
        OnBoxHitAfterThrow += TouchDestroy;
    }

    private void TouchDestroy()
    {
        DestroyBox();
    }

    private void PlayerHit(GameObject player)
    {
        player.GetComponent<PlayerController>().ApplyDamage(m_Damage);
        DestroyBox();
    }
}