﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxFactory
{
    static GameObject m_FloorBox;
    static GameObject m_BaseBox;

    static Material m_FloorBoxMaterial;
    static Material m_WallBoxMaterial;

    static Material m_WoodBoxMaterial;
    static Material m_TNTBoxMaterial;
    static Material m_NitroBoxMaterial;

    static bool m_bIsInitialized = false;

    public static void Initialize()
    {
        if(!m_bIsInitialized)
        {
            m_FloorBoxMaterial = Resources.Load<Material>("Materials/FloorCubeMaterial");
            m_WallBoxMaterial = Resources.Load<Material>("Materials/WallCubeMaterial");
            m_WoodBoxMaterial = Resources.Load<Material>("Materials/WoodBox");
            m_TNTBoxMaterial = Resources.Load<Material>("Materials/TNTBox");
            m_NitroBoxMaterial = Resources.Load<Material>("Materials/NitroBox");

            m_FloorBox = Resources.Load<GameObject>("Meshes/SmoothCube");
            m_BaseBox = Resources.Load<GameObject>("Prefabs/BaseBox");
        }
    }

    public static GameObject CreateBox(BoxType type)
    {
        switch (type)
        {
            default:
                return null;

            case BoxType.Floor:
                GameObject floorCubeInstance = Object.Instantiate(m_FloorBox);
                floorCubeInstance.GetComponent<MeshRenderer>().material = m_FloorBoxMaterial;
                return floorCubeInstance;

            case BoxType.Wall:
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.localScale = new Vector3(1f, 2f, 1f);
                cube.GetComponent<MeshRenderer>().material = m_WallBoxMaterial;
                Object.Destroy(cube.GetComponent<BoxCollider>());
                return cube;

            case BoxType.Wood:
                cube = Object.Instantiate(m_BaseBox);
                cube.GetComponent<Renderer>().material = m_WoodBoxMaterial;
                cube.AddComponent<WoodBox>().SetUpBox(BoxType.Wood);
                return cube;

            case BoxType.TNT:
                cube = Object.Instantiate(m_BaseBox);
                cube.GetComponent<Renderer>().material = m_TNTBoxMaterial;
                cube.AddComponent<TNTBox>().SetUpBox(BoxType.TNT);
                return cube;

            case BoxType.Nitro:
                cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.AddComponent<Rigidbody>();
                cube.GetComponent<Renderer>().material = m_NitroBoxMaterial;
                cube.AddComponent<NitroBox>().SetUpBox(BoxType.Nitro);
                return cube;
        }
    }
}
