﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;

public class GameManager : MonoBehaviour
{
    public static bool GamePaused = false;
    public delegate void OnUnPause(float timeDiff);
    public static event OnUnPause GameUnPaused;

    [SerializeField]
    PlayerController m_PlayerPrefab;

    [SerializeField]
    AIController m_AIPrefab;

    [SerializeField]
    Vector3[] m_SpawnPositions = new Vector3[4];

    [SerializeField]
    PlatformSpawner m_PlatformSpawner;

    [SerializeField]
    PlatformWaveController m_PlatformWaveController;

    [SerializeField]
    PostProcessingProfile[] m_PostProcessingProfiles = new PostProcessingProfile[2];

    MatchData m_MatchData;
    BoxSpawner m_BoxSpawner;
    Quaternion m_PlayerInitialRotation = Quaternion.Euler(0f, 180f, 0f);

    public static List<PlayerController> PlayerList;
    List<PlayerController> m_PlayerList = new List<PlayerController>();
    AIProfile[] m_AIProfiles;

    float m_MatchStartTime = 0f;
    int m_PlayerDeathCount = 0;
    bool m_bMatchStarted = false;
    bool m_SuddenDeath = false;
    UIManager m_UIManager = null;
    AudioManager m_AudioManager = null;
    PostProcessingBehaviour m_PostProcessingBehaviour = null;

    float m_TimeAtPause = 0f;
    BaseBox[] m_BoxesAtPause = null;

    void Awake()
    {
        BoxFactory.Initialize();

        m_UIManager = UIManager.Instance;
        m_AudioManager = AudioManager.Instance;
        m_MatchData = MatchData.Instance;
        m_BoxSpawner = GetComponent<BoxSpawner>();
        m_PostProcessingBehaviour = GetComponent<PostProcessingBehaviour>();
        m_PlatformSpawner.OnFloorSpawnFinished += PlatformFinishedSpawning;
        m_PlatformSpawner.OnFloorOnPosition += PlatformOnPosition;
        m_AIProfiles = Resources.LoadAll<AIProfile>("AI Profiles");

        m_PostProcessingBehaviour.profile = m_PostProcessingProfiles[0];
    }

    void Start()
    {
        m_PlatformSpawner.SpawnFloor();
    }

    void Update()
    {
        if (m_bMatchStarted && !GamePaused)
        {
            float currentMatchTime = 90f - (Time.time - m_MatchStartTime);
            TimeSpan time = TimeSpan.FromSeconds(currentMatchTime);
            m_UIManager.UpdateMatchTimer(time.Minutes, time.Seconds);

            if(!m_SuddenDeath && currentMatchTime <= 30)
            {
                m_SuddenDeath = true;
                StartSuddenDeath();
            }

            if(currentMatchTime <= 0)
            {
                m_UIManager.UpdateMatchTimer(0, 0);
                EndMatch();
            }
        }

        if (m_bMatchStarted && Input.GetButtonDown("Pause"))
        {
            if (!GamePaused)
                Pause();
            else
                UnPause();
        }
    }

    void Pause()
    {
        GamePaused = true;
        m_UIManager.ShowPause();
        m_TimeAtPause = Time.time;
        foreach (PlayerController pController in m_PlayerList)
            pController.enabled = false;

        m_BoxesAtPause = FindObjectsOfType<BaseBox>();
        foreach(BaseBox box in m_BoxesAtPause)
        {
            box.enabled = false;
            Rigidbody rigidBody = box.GetComponent<Rigidbody>();
            if (rigidBody != null)
                rigidBody.isKinematic = true;
        }
    }

    void UnPause()
    {
        float timeDiff = Time.time - m_TimeAtPause;
        m_MatchStartTime += timeDiff;
        GameUnPaused(timeDiff);

        m_UIManager.HidePause();
        foreach (PlayerController pController in m_PlayerList)
            pController.enabled = true;

        foreach (BaseBox box in m_BoxesAtPause)
        {
            Rigidbody rigidBody = box.GetComponent<Rigidbody>();
            if (rigidBody != null)
                rigidBody.isKinematic = false;
            box.enabled = true;
        }

        GamePaused = false;
    }

    void PlatformOnPosition()
    {
        SpawnPlayers();
        m_BoxSpawner.Initialize();
    }

    void PlatformFinishedSpawning()
    {
        StartCoroutine(MatchCountdown());
    }

    void SpawnPlayers()
    {
        for (int i = 0; i < m_MatchData.humansCount; ++i)
        {
            PlayerController prefab = Instantiate(m_PlayerPrefab, m_SpawnPositions[i], m_PlayerInitialRotation);
            prefab.SetupPlayerData(i, m_MatchData.playerInputStrings[i], PlayerDeathCallback);

            prefab.GetComponent<Renderer>().material.color = m_MatchData.playerColors[i];

            m_UIManager.SetupHealthBar(prefab, m_MatchData.playerColors[i]);
            m_PlayerList.Add(prefab);
        }
        
        for (int i = m_MatchData.humansCount; i < 4; ++i)
        {
            AIController prefab = Instantiate(m_AIPrefab, m_SpawnPositions[i], m_PlayerInitialRotation);
            prefab.name = i.ToString();
            prefab.SetupPlayerData(i, string.Empty, PlayerDeathCallback);
            prefab.SetProfile(m_AIProfiles[UnityEngine.Random.Range(0, m_AIProfiles.Length)]);

            prefab.GetComponent<Renderer>().material.color = m_MatchData.playerColors[i];

            m_UIManager.SetupHealthBar(prefab, m_MatchData.playerColors[i]);
            m_PlayerList.Add(prefab);
        }

        PlayerList = m_PlayerList;
    }

    void PlayerDeathCallback(int playerNum)
    {
        ++m_PlayerDeathCount;
        if (m_PlayerDeathCount == 3)
            EndMatch();
    }

    void EndMatch()
    {
        foreach (PlayerController pController in m_PlayerList)
            pController.enabled = false;

        m_bMatchStarted = false;
        m_PlatformWaveController.enabled = false;
        m_PlatformSpawner.StopSuddenDeathCoroutine();
        m_PlatformSpawner.enabled = false;
        m_BoxSpawner.StopSpawner();

        if(m_PlayerDeathCount == 3)
        {
            foreach (PlayerController pController in m_PlayerList)
                if (pController.IsAlive)
                    m_UIManager.SetWinText(pController.GetPlayerNum());
        }
        else
            m_UIManager.SetWinText(-1);
    }

    IEnumerator MatchCountdown()
    {
        for (int i = 0; i <= 3; ++i)
        {
            yield return new WaitForSeconds(1.5f);
            if(i == 3)
                m_AudioManager.PlayOneShot(AudioClipType.Countdown, 1f, 1.5f);
            else
                m_AudioManager.PlayOneShot(AudioClipType.Countdown);
            m_UIManager.UpdateMatchCountdown(3 - i);
        }

        m_MatchStartTime = Time.time;
        m_bMatchStarted = true;
        m_BoxSpawner.StartSpawner();

        foreach (PlayerController pController in m_PlayerList)
            pController.SetAlive();

        yield return new WaitForSeconds(4f);
        m_PlatformWaveController.StartGroundPound();
    }

    void StartSuddenDeath()
    {
        m_AudioManager.SetSuddenDeath();
        m_UIManager.DisplaySuddenDeathText();
        m_PostProcessingBehaviour.profile = m_PostProcessingProfiles[1];
        m_PlatformWaveController.StartSuddenDeath();
        m_PlatformSpawner.StartSuddenDeathCoroutine();
    }

    public void Rematch()
    {
        m_AudioManager.PlayOneShot(AudioClipType.UI_Click);
        StartCoroutine(WaitforRematch());
    }

    public void GoToMenu()
    {
        m_AudioManager.PlayOneShot(AudioClipType.UI_Click);
        StartCoroutine(WaitforMenu());
    }

    IEnumerator WaitforRematch()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator WaitforMenu()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(0);
    }
}
