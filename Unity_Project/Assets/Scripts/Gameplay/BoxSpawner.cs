﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour
{
    [System.Serializable]
    struct BoxSpawnRates
    {
        public int m_WoodBoxesSpawnRate;
        public int m_TNTBoxesSpawnRate;
        public int m_NitroBoxesSpawnRate;
    }

    [SerializeField]
    Transform m_BoxesParent;

    [SerializeField]
    int m_MaxBoxesInSingleBurst = 0;

    [SerializeField]
    float m_SpawnInterval = 0;

    [SerializeField]
    float m_SpawnIntervalMaxRandomDeviation = 0;

    [Header("Box spawn rates in %.")][SerializeField]
    BoxSpawnRates m_SpawnRates;

    int m_BoxCollisionMask;
    float m_NextBoxSpawnWait = 0f;
    bool m_IsSpawningBoxes = false;

    void Awake()
    {
        m_BoxCollisionMask = 1 << LayerMask.NameToLayer("Boxes") | 1 << LayerMask.NameToLayer("Player");
#if UNITY_EDITOR
        int sum = 0;
        sum += m_SpawnRates.m_WoodBoxesSpawnRate;
        sum += m_SpawnRates.m_TNTBoxesSpawnRate;
        sum += m_SpawnRates.m_NitroBoxesSpawnRate;

        if (sum != 100)
        {
            Debug.LogError("Spawn rates dont add up tp 100%");
            Debug.Break();
        }
#endif
        m_SpawnRates.m_TNTBoxesSpawnRate += m_SpawnRates.m_WoodBoxesSpawnRate;
        m_SpawnRates.m_NitroBoxesSpawnRate += m_SpawnRates.m_TNTBoxesSpawnRate;  
    }

    public void Initialize()
    {
        SpawnInitialBoxes();
    }

    public void StartSpawner()
    {
        m_IsSpawningBoxes = true;
        m_NextBoxSpawnWait = m_SpawnInterval + Random.Range(0f, m_SpawnIntervalMaxRandomDeviation);
        StartCoroutine(BoxSpawn());
    }

    public void StopSpawner()
    {
        m_IsSpawningBoxes = false;
    }

    void SpawnInitialBoxes()
    {
        StartCoroutine(SpawnBox(BoxType.Wood));
        StartCoroutine(SpawnBox(BoxType.Wood));
        StartCoroutine(SpawnBox(BoxType.Wood));
        StartCoroutine(SpawnBox(BoxType.Wood));
        StartCoroutine(SpawnBox(BoxType.Wood));
        StartCoroutine(SpawnBox(BoxType.Wood));

        StartCoroutine(SpawnBox(BoxType.TNT));
        StartCoroutine(SpawnBox(BoxType.TNT));
        StartCoroutine(SpawnBox(BoxType.TNT));
        StartCoroutine(SpawnBox(BoxType.TNT));

        StartCoroutine(SpawnBox(BoxType.Nitro));
        StartCoroutine(SpawnBox(BoxType.Nitro));
    }

    IEnumerator BoxSpawn()
    {
        yield return new WaitForSeconds(m_NextBoxSpawnWait);

        while (GameManager.GamePaused)
            yield return null;

        m_NextBoxSpawnWait = m_SpawnInterval + Random.Range(0f, m_SpawnIntervalMaxRandomDeviation);

        int boxesToSpawn = Random.Range(1, m_MaxBoxesInSingleBurst + 1);
        for(int i = 0; i < boxesToSpawn; ++i)
        {
            int random = Random.Range(0, 101);
            if (random >= 0 && random <= m_SpawnRates.m_WoodBoxesSpawnRate)
                StartCoroutine(SpawnBox(BoxType.Wood));
            else if (random >= m_SpawnRates.m_WoodBoxesSpawnRate && random <= m_SpawnRates.m_TNTBoxesSpawnRate)
                StartCoroutine(SpawnBox(BoxType.TNT));
            else if (random >= m_SpawnRates.m_TNTBoxesSpawnRate && random <= m_SpawnRates.m_NitroBoxesSpawnRate)
                StartCoroutine(SpawnBox(BoxType.Nitro));
        }

        if (m_IsSpawningBoxes)
            StartCoroutine(BoxSpawn());
    }

    IEnumerator SpawnBox(BoxType type)
    {
        Vector3 cubePosition = new Vector3(Random.Range(-11.5f, 11.5f), 1f, Random.Range(-11.5f, 11.5f));
        while (Physics.CheckSphere(cubePosition, 1.5f, m_BoxCollisionMask) ||
            (cubePosition.x >= -3f && cubePosition.x <= 3f && cubePosition.z >= -3f && cubePosition.z <= 3f))
        {
            cubePosition = new Vector3(Random.Range(-11.5f, 11.5f), 1f, Random.Range(-11.5f, 11.5f));
            yield return null;
        }

        GameObject cube = BoxFactory.CreateBox(type);
        cube.transform.position = cubePosition;
        cube.transform.parent = m_BoxesParent;
    }
}
