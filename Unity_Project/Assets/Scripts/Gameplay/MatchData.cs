﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchData
{
    private static MatchData m_Instance;
    public static MatchData Instance
    {
        get
        {
            return m_Instance ?? (m_Instance = new MatchData());
        }
    }

    public int humansCount = 0;
    public Color[] playerColors = new Color[4];
    public string[] playerInputStrings = new string[4];

    private MatchData()
    {
        playerColors[0] = Color.yellow;
        playerColors[1] = Color.red;
        playerColors[2] = Color.cyan;
        playerColors[3] = Color.green;

        //Testing
        humansCount = 2;
        playerInputStrings[0] = "_K1";
        playerInputStrings[1] = "_K2";
        playerInputStrings[2] = "_J1";
        playerInputStrings[3] = "_J2";
    }
}